![](logo.png)
<!-- the below links we redirect to actual experiment on vlabs website-->
[AIM](http://vlabs.iitb.ac.in/vlabs-dev/labs/cglab/labs/two-d-object-rotation-pvg/index.html) &nbsp;[THEORY](http://vlabs.iitb.ac.in/vlabs-dev/labs/cglab/labs/two-d-object-rotation-pvg/theory.html) &nbsp;[PROCEDURE](http://vlabs.iitb.ac.in/vlabs-dev/labs/cglab/labs/two-d-object-rotation-pvg/procedure.html)  
# **Aim:**  
To study the <b>Rotation</b> Transformation performed on 2-D graphics object.

# **Theory:**
* A rotation of an object is used to rotate an object in either clockwise or anticlockwise direction.
* A rotation repositions all points of an object along a circular path in the plane centered at the origin or arbitrary point.
* The rotation transformation is needed to manipulate the initial object coordinate and display the modified object coordinate with the help of rotation angle (Ɵ) ,with respect to the origin or the arbitrary point as shown in figure below:
  
 ![](theoryImage1.png)


* Consider a point P(x,y) is rotated in anticlockwise direction by an angle ‘Ɵ’. The point P(x,y) is represented as
  
![](theoryImage2.png)

<span style="color:blue">x = R * cos f</span>

<span style="color:blue">y = R * sin f</span>
* The Point P’(x’,y’) after performing the rotation in anticlockwise direction by an angle ‘Ɵ’ is represented as
  
![](theoryImage3.png)

<span style="color:blue">x’ = x * cos q – y * sin q</span>

<span style="color:blue">y’ = x * sin q + y * cos q</span>
* The Point P(x,y) is rotated in clockwise direction by an angle ‘Ɵ’, then replace ‘Ɵ’ by ’- Ɵ’; and simplify the equation.
Rotation transformation can be performed :
*  About the origin :
If the point p (x,y) is rotated about origin with an angle ‘Ɵ’ in anticlockwise direction; then we can write the coordinate of P’(x’,y’) as:

<span style="color:blue">x’ = x * cos q – y * sin q</span>

<span style="color:blue">y’ = x * sin q + y * cos q</span>
* About an arbitrary point P (px,py) :
1. To perform the rotation about the arbitrary point ‘P’ we have to follow the steps listed below.
2. Translate the 2-D object so that arbitrary point ‘P’ will coincide with the origin, by performing the translation transformation with translation factor T(-px, -py)
3. Rotate the object with respect to origin, with rotation angle ‘Ɵ’ in either clockwise or anticlockwise direction respectively.
4. Inverse Translate the object with respect to the arbitrary point ‘P’ by performing translation transformation with translation factor T(px,py)
5. Rotation can be represented with the use of Homogenous coordinate system using matrix representation as
   
![](theoryImage4.png)

where, P(x ,y) coordinate will be rotated to P’(x’, y’) with respect to the angle ‘Ɵ’; in anticlockwise direction.


# **Procedure:**
1. Start.
2. Accept coordinates to construct a 2-D object.
3. Display the 2-D object.
4. Construct the Homogeneous matrix for the object with reference to the coordinate of the object.
5. Accept the angle of rotation ‘Ɵ’, with reference to the coordinate system in wither clockwise or anticlockwise direction.
6. Construct the rotated 2-D object with an angle ‘Ɵ’; with the use of Homogeneous matrix described earlier.
7. Plot rotated object (x’, y’) w.r.t.Homogeneous coordinates.
8. Stop.
var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function check() {
    var a = parseFloat(t1.value);
    var b = parseFloat(t2.value);
    var c = parseFloat(t3.value);
    if (a != b && b != c) {
        if ((Math.pow(a, 2) + Math.pow(b, 2)) == Math.pow(c, 2) || (Math.pow(c, 2) + Math.pow(b, 2)) == Math.pow(a, 2) || (Math.pow(a, 2) + Math.pow(c, 2)) == Math.pow(b, 2)) {
            alert("the triangle is scalene and right angled ");
        }
        else {
            alert("the triangle is scalene");
        }
    }
    else if (a == b && b == c) {
        alert("the triangle is equilateral");
    }
    else if ((a == b && b != c) || (c == b && a != c) || (a == c && a != b)) {
        if ((Math.pow(a, 2) + Math.pow(b, 2)) == Math.pow(c, 2) || (Math.pow(c, 2) + Math.pow(b, 2)) == Math.pow(a, 2) || (Math.pow(a, 2) + Math.pow(c, 2)) == Math.pow(b, 2)) {
            alert("the triangle is isoceles & right angled");
        }
        else {
            alert("the triangle is isoceles");
        }
    }
}
//# sourceMappingURL=typetriangle.js.map